# Agenda_Personal

Agenda para registro de telefonos en Lua GTK y Sqlite3

## Todo
- [x] keyval hablitilado en modulo de registro de usuarios
- [x] dialogo de ayuda
- [x] tray icon
- [x] notificaciones
- [ ] boton limpiar implementado
- [ ] buscador de contactos implementado
- [ ] añadido el submodulo de sqlite3 
- [x] añadido el submodulo de libnotify

## Screenshot

![registro-de-usuario](screenshot/registro-de-usuario.png) 

![login](screenshot/login.png) 

![registro-de-contactos](screenshot/registro-de-contactos.png) 


## Dependencias

- [sqlite3](https://www.sqlite.org/download.html)
- [lua-lgi](https://github.com/pavouk/lgi/)
- [libnotify](https://github.com/GNOME/libnotify)

### Instalacion
`make db && make install`

#### Contactos

Gmail: sodomon2@gmail.com
